package com.js.insuranceconsultationapi.service;

import ch.qos.logback.core.util.COWArrayList;
import com.js.insuranceconsultationapi.entity.Consultation;
import com.js.insuranceconsultationapi.enums.Gender;
import com.js.insuranceconsultationapi.model.ConsultationItem;
import com.js.insuranceconsultationapi.model.ConsultationRequest;
import com.js.insuranceconsultationapi.model.ConsultationResponse;
import com.js.insuranceconsultationapi.repository.ConsultationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ConsultationService {
    private final ConsultationRepository consultationRepository;

    public void setConsultation(ConsultationRequest request){
        Consultation addData = new Consultation();
        addData.setDateMaker(LocalDate.now());
        addData.setName(request.getName());
        addData.setBirthDay(request.getBirthDay());
        addData.setMobile(request.getMobile());
        addData.setGender(request.getGender());

        consultationRepository.save(addData);
    }

    public List<ConsultationItem> getConsultations() {
        List<Consultation> originlist = consultationRepository.findAll();
        List<ConsultationItem> result = new LinkedList<>();

        for (Consultation consultation : originlist) {
            ConsultationItem addItem = new ConsultationItem();
            addItem.setId(consultation.getId());
            addItem.setDateMaker(consultation.getDateMaker());
            addItem.setName(consultation.getName());
            addItem.setBirthDay(consultation.getBirthDay());
            addItem.setGender(consultation.getGender().getGenders());

            result.add(addItem);
        }
        return result;
    }

    public ConsultationResponse getConsultation(Long id){
        Consultation originData = consultationRepository.findById(id).orElseThrow();
        ConsultationResponse response = new ConsultationResponse();

        response.setId(originData.getId());
        response.setDateMaker(originData.getDateMaker());
        response.setName(originData.getName());
        response.setBirthDay(originData.getBirthDay());
        response.setMobile(originData.getMobile());
        response.setGender(originData.getGender().getGenders());

        return response;
    }

    public List<ConsultationItem> getGender(){
        List<Consultation> alllist = consultationRepository.findAll();
        List<ConsultationItem> genderresult = new LinkedList<>();

        for (Consultation consultation : alllist) {
            if (consultation.getGender().equals(Gender.MAN)) {
                ConsultationItem addItem = new ConsultationItem();
                addItem.setId(consultation.getId());
                addItem.setDateMaker(consultation.getDateMaker());
                addItem.setName(consultation.getName());
                addItem.setBirthDay(consultation.getBirthDay());
                addItem.setGender(consultation.getGender().getGenders());

                genderresult.add(addItem);
            }
        }
        return genderresult;
    }
}
