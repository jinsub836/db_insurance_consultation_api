package com.js.insuranceconsultationapi.controller;

import com.js.insuranceconsultationapi.model.ConsultationItem;
import com.js.insuranceconsultationapi.model.ConsultationRequest;
import com.js.insuranceconsultationapi.model.ConsultationResponse;
import com.js.insuranceconsultationapi.service.ConsultationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/consultation")
public class ConsultationController {
    private final ConsultationService consultationService;

    @PostMapping("/new")
    public String setConsultation(@RequestBody ConsultationRequest request){
        consultationService.setConsultation(request);
        return "보험 상담 신청이 완료되었습니다.";
    }

    @GetMapping("/all")
    public List<ConsultationItem> getConsultations(){ return consultationService.getConsultations();
    }

    @GetMapping("/detail/{id}")
    public ConsultationResponse getConsultation(@PathVariable long id) {
        return consultationService.getConsultation(id);
    }

    @GetMapping("/gender")
    public List<ConsultationItem> getGender(){ return consultationService.getGender();
    }
}
