package com.js.insuranceconsultationapi.repository;

import com.js.insuranceconsultationapi.entity.Consultation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConsultationRepository extends JpaRepository<Consultation , Long> {
}
