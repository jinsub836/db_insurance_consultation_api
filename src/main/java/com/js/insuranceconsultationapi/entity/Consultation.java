package com.js.insuranceconsultationapi.entity;

import com.js.insuranceconsultationapi.enums.Gender;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Consultation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate dateMaker;

    @Column(nullable = false , length = 20)
    private String name;

    @Column(nullable = false)
    private Integer birthDay;

    @Column(nullable = false, length = 13)
    private String mobile;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 6)
    private Gender gender;
}
