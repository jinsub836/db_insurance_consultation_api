package com.js.insuranceconsultationapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InsuranceConsultationApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(InsuranceConsultationApiApplication.class, args);
    }

}
