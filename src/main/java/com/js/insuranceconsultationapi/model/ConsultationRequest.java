package com.js.insuranceconsultationapi.model;

import com.js.insuranceconsultationapi.enums.Gender;
import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConsultationRequest {

    private String name;

    private Integer birthDay;

    private String mobile;

    private Gender gender;
}

