package com.js.insuranceconsultationapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ConsultationResponse {
    private Long id;

    private LocalDate dateMaker;

    private String name;

    private Integer birthDay;

    private String mobile;

    private String gender;
}
