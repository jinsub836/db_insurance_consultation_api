package com.js.insuranceconsultationapi.model;

import com.js.insuranceconsultationapi.enums.Gender;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ConsultationItem {

    private Long id;

    private LocalDate dateMaker;

    private String name;

    private Integer birthDay;

    private String gender;

}
